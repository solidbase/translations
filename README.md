# SolidBase translations

This repository holds the translations of the SolidBase.info app.

These are maintained from https://weblate.allmende.io/projects/solidbase/solidbase/

The main repository of the application is https://lab.allmende.io/solidbase/solidbase

It is licenced under the GNU AGPL v3. See LICENCE for details.

(c) Solidarische Landwirtschaft e.V.
